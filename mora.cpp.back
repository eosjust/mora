#include "mora.hpp"

void mora::hi(account_name user)
{
  require_auth(user);
  print("Hello, ", name{user});
}

void mora::opennewgame(account_name player, asset quantity)
{
  require_auth(player);
  eosio_assert(quantity.symbol == string_to_symbol(4, "MORA"), "unsupported symbol name");
  eosio_assert(quantity.is_valid(), "invalid quantity");
  eosio_assert(quantity.amount > 0, "must deposit positive quantity");
  mtransfer(player, _self, quantity, player);

  allgames allgametable(_self, _self);
  opengames opengametable(_self, _self);
  mygames mygametable(_self, player);
  historygames historygametable(_self, player);
  time nowtime = now();
  uint64_t gameid = allgametable.available_primary_key();
  allgametable.emplace(player, [&](auto &game) {
    game.id = gameid;
    game.player1 = player;
    game.createtime = nowtime;
    game.balance = quantity;
  });
  opengametable.emplace(player, [&](auto &game) {
    game.id = gameid;
    game.player1 = player;
    game.createtime = nowtime;
    game.balance = quantity;
  });
  mygametable.emplace(player, [&](auto &game) {
    game.id = gameid;
    game.player1 = player;
    game.createtime = nowtime;
    game.balance = quantity;
  });
  historygametable.emplace(player, [&](auto &game) {
    game.id = gameid;
    game.player1 = player;
    game.createtime = nowtime;
    game.balance = quantity;
  });
}

void mora::joingame(uint64_t gameid, account_name player2)
{
  eosio_assert(is_account(player2), "player2 account does not exist");
  require_auth(player2);

  time nowtime = now();
  allgames allgametable(_self, _self);
  opengames opengametable(_self, _self);
  rungames rungametable(_self, _self);
  auto itemallgame = allgametable.find(gameid);
  auto itemopengame = opengametable.find(gameid);
  eosio_assert(itemallgame != allgametable.end(), "there isnt any game");
  eosio_assert(is_account(itemallgame->player1), "player1 account does not exist");
  eosio_assert(itemallgame->player1 != player2, "cant play with self");
  eosio_assert(!is_account(itemallgame->player2), "this game's player2 is not null");
  account_name player1 = itemallgame->player1;
  mygames mygametable1(_self, player1);
  mygames mygametable2(_self, player2);
  historygames historygametable1(_self, player1);
  historygames historygametable2(_self, player2);
  auto itemmygame1 = mygametable1.find(gameid);
  auto itemhistorygame1 = historygametable1.find(gameid);
  eosio_assert(itemmygame1 != mygametable1.end(), "player1 game not found");
  eosio_assert(itemhistorygame1 != historygametable1.end(), "game history not found");
  //allgame,opengame,rungame,mygame,historygame
  allgametable.modify(itemallgame, _self, [&](auto &a) {
    a.player2 = player2;
    a.starttime = nowtime;
  });
  opengametable.erase(itemopengame);
  rungametable.emplace(player2, [&](auto &a) {
    a.id = itemallgame->id;
    a.player1 = itemallgame->player1;
    a.player2 = itemallgame->player2;
    a.balance = itemallgame->balance;
    a.createtime = itemallgame->createtime;
    a.starttime = nowtime;
  });
  mygametable1.modify(itemmygame1, player1, [&](auto &a) {
    a.player2 = player2;
    a.starttime = nowtime;
  });
  historygametable1.modify(itemhistorygame1, player1, [&](auto &a) {
    a.player2 = player2;
    a.starttime = nowtime;
  });
  mygametable2.emplace(player2, [&](auto &a) {
    a.id = itemallgame->id;
    a.player1 = itemallgame->player1;
    a.player2 = itemallgame->player2;
    a.balance = itemallgame->balance;
    a.createtime = itemallgame->createtime;
    a.starttime = nowtime;
  });
  historygametable2.emplace(player2, [&](auto &a) {
    a.id = itemallgame->id;
    a.player1 = itemallgame->player1;
    a.player2 = itemallgame->player2;
    a.balance = itemallgame->balance;
    a.createtime = itemallgame->createtime;
    a.starttime = nowtime;
  });
}

void mora::jurgegame(uint64_t gameid, account_name winner)
{
  eosio_assert(is_account(winner), "winner account does not exist");
  require_auth(winner);

  time nowtime = now();
  allgames allgametable(_self, _self);
  rungames rungametable(_self, _self);
  auto itemallgame = allgametable.find(gameid);
  auto itemrungame = rungametable.find(gameid);
  eosio_assert(itemallgame != allgametable.end(), "there isnt any game");
  eosio_assert(is_account(itemallgame->player1), "player1 account does not exist");
  eosio_assert(is_account(itemallgame->player2), "player2 account does not exist");
  eosio_assert(!is_account(itemallgame->winner), "this game already has a winner");
  eosio_assert(itemallgame->player1 != itemallgame->player2, "cant play with self");

  if (winner == itemallgame->player1)
  {
  }
  else if (winner == itemallgame->player2)
  {
  }
  else
  {
    eosio_assert(false, "input winner is not in this game");
  }
  eosio_assert(itemrungame != rungametable.end(), "this game is not running");
  rungametable.erase(itemrungame);
  //
  account_name player1 = itemallgame->player1;
  account_name player2 = itemallgame->player2;
  mygames mygametable1(_self, player1);
  mygames mygametable2(_self, player2);
  historygames historygametable1(_self, player1);
  historygames historygametable2(_self, player2);
  auto itemmygame1 = mygametable1.find(gameid);
  auto itemmygame2 = mygametable2.find(gameid);
  auto itemhistorygame1 = historygametable1.find(gameid);
  auto itemhistorygame2 = historygametable2.find(gameid);
  eosio_assert(itemmygame1 != mygametable1.end(), "player1's game does not exist");
  eosio_assert(itemmygame2 != mygametable2.end(), "player2's game does not exist");
  eosio_assert(itemhistorygame1 != historygametable1.end(), "player1's history does not exist");
  eosio_assert(itemhistorygame2 != historygametable2.end(), "player2's history does not exist");
  mygametable1.erase(itemmygame1);
  mygametable2.erase(itemmygame2);
  //
  allgametable.modify(itemallgame, _self, [&](auto &a) {
    a.winner = winner;
    a.endtime = nowtime;
  });
  historygametable1.modify(itemhistorygame1, player1, [&](auto &a) {
    a.winner = winner;
    a.endtime = nowtime;
  });
  historygametable2.modify(itemhistorygame2, player2, [&](auto &a) {
    a.winner = winner;
    a.endtime = nowtime;
  });
}

void mora::claimad(account_name account)
{
  require_auth(account);
  asset airdrop_claim_quantity = asset(10000000, string_to_symbol(4, "MORA"));
  time airdrop_claim_interval = 100;
  time airdrop_start_time = 1531908000;
  time airdrop_end_time = 1633117600;
  eosio_assert(now() >= airdrop_start_time, "Airdrop has not started");
  eosio_assert(now() < airdrop_end_time, "Airdrop is ended");
  accstates accstates_table(_self, _self);
  auto accstates_itr = accstates_table.find(account);
  if (accstates_itr != accstates_table.end())
  {
    eosio_assert(now() >= accstates_itr->last_airdrop_claim_time + airdrop_claim_interval, "claim is too frequent");
  }

  if (accstates_itr == accstates_table.end())
  {
    accstates_table.emplace(account, [&](auto &a) {
      a.account = account;
      a.last_airdrop_claim_time = now();
      a.eos_balance = asset(0, string_to_symbol(4, "EOS"));
    });
  }
  else
  {
    accstates_table.modify(accstates_itr, account, [&](auto &a) {
      a.account = account;
      a.last_airdrop_claim_time = now();
    });
  }
  mtransfer(_self, account, airdrop_claim_quantity, account);
}

//only for test
void mora::erasegame1(account_name player)
{
}

//@abi action
void mora::deposit(const account_name from, const asset &quantity)
{

  eosio_assert(quantity.is_valid(), "invalid quantity");
  eosio_assert(quantity.amount > 0, "must deposit positive quantity");

  action(
      permission_level{from, N(active)},
      N(eosio.token), N(transfer),
      std::make_tuple(from, _self, quantity, std::string("")))
      .send();
}

void mora::modifystr(account_name receiver)
{
  auto table1 = N(table1);

  int alice_itr = db_store_i64(receiver, table1, receiver, N(alice), "alice's info", strlen("alice's info"));
  db_store_i64(receiver, table1, receiver, N(bob), "bob's info", strlen("bob's info"));
  db_store_i64(receiver, table1, receiver, N(charlie), "charlie's info", strlen("charlies's info"));
  db_store_i64(receiver, table1, receiver, N(allyson), "allyson's info", strlen("allyson's info"));

  uint64_t prim = 0;
  int itr_next = db_next_i64(alice_itr, &prim);
  int itr_next_expected = db_find_i64(receiver, receiver, table1, N(allyson));
  eosio_assert(itr_next == itr_next_expected && prim == N(allyson), "primary_i64_general - db_find_i64");
  itr_next = db_next_i64(itr_next, &prim);
  itr_next_expected = db_find_i64(receiver, receiver, table1, N(bob));
  eosio_assert(itr_next == itr_next_expected && prim == N(bob), "primary_i64_general - db_next_i64");
}

void mora::fetchstr(account_name receiver)
{
  auto table1 = N(table1);
  int itr = db_find_i64(receiver, receiver, table1, N(bob));
  eosio_assert(itr >= 0, "itr is < 0");
  uint32_t buffer_len = 5;
  char value[50];
  auto len = db_get_i64(itr, value, buffer_len);
  value[buffer_len] = '\0';
  std::string s(value);
  eosio_assert(s == "bob's", "primary_i64_general - db_get_i64  - 5");
  eosio_assert(false, s.c_str());
}
void mora::apply(uint64_t code, uint64_t action)
{
  if (action == N(transfer))
  {
    on(unpack_action_data<currency::transfer>(), code);
    return;
  }
}
void mora::on(const currency::transfer &t, account_name code)
{

}

void mora::apply(account_name contract, account_name act) {

  if (act == N(transfer)) {
    on(unpack_action_data<currency::transfer>(), contract);
    return;
  }

  if (contract != _this_contract)
    return;

  auto &thiscontract = *this;
  switch (act) { EOSIO_API(mora, (hi)(opennewgame)(joingame)(jurgegame)(claimad)(erasegame1)(deposit)(modifystr)(fetchstr)); };
}

extern "C"
{
  [[noreturn]] void apply(uint64_t receiver, uint64_t code, uint64_t action) {
    mora gtb(receiver);
    gtb.apply(code, action);
    eosio_exit(0);
  }
}