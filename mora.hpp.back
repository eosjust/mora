#include <utility>
#include <vector>
#include <string>
#include <eosiolib/eosio.hpp>
#include <eosiolib/currency.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/dispatcher.hpp>
#include <eosiolib/multi_index.hpp>
#include <eosiolib/types.hpp>
#include <eosiolib/action.hpp>
#include <eosiolib/transaction.hpp>
#include <eosiolib/datastream.hpp>
#include <eosiolib/db.h>
#include <eosiolib/memory.hpp>
using namespace eosio;
using namespace std;
class mora : public eosio::contract
{
  private:
    account_name _this_contract;
    //@abi table
    struct account
    {
        asset balance;
        uint64_t primary_key() const { return balance.symbol.name(); }
    };
    typedef eosio::multi_index<N(accounts), account> accounts;
    //@abi table
    struct accstate
    {
        account_name account;
        time last_airdrop_claim_time;
        asset eos_balance;

        uint64_t primary_key() const { return account; }

        EOSLIB_SERIALIZE(accstate,
                         (account)(last_airdrop_claim_time)(eos_balance))
    };
    typedef eosio::multi_index<N(accstates), accstate> accstates;

    //@abi table 所有游戏记录
    struct allgame
    {
        uint64_t id;
        account_name player1;
        account_name player2;
        asset balance;
        account_name winner;
        time createtime;
        time starttime;
        time endtime;
        uint64_t primary_key() const { return id; }
        EOSLIB_SERIALIZE(allgame, (id)(player1)(player2)(balance)(winner)(createtime)(starttime)(endtime))
    };
    typedef eosio::multi_index<N(allgame), allgame> allgames;

    //@abi table 待加入的游戏
    struct opengame
    {
        uint64_t id;
        account_name player1;
        account_name player2;
        asset balance;
        account_name winner;
        time createtime;
        time starttime;
        time endtime;
        uint64_t primary_key() const { return id; }
        EOSLIB_SERIALIZE(opengame, (id)(player1)(player2)(balance)(winner)(createtime)(starttime)(endtime))
    };
    typedef eosio::multi_index<N(opengame), opengame> opengames;

    //@abi table 正在进行的游戏
    struct rungame
    {
        uint64_t id;
        account_name player1;
        account_name player2;
        asset balance;
        account_name winner;
        time createtime;
        time starttime;
        time endtime;
        uint64_t primary_key() const { return id; }
        EOSLIB_SERIALIZE(rungame, (id)(player1)(player2)(balance)(winner)(createtime)(starttime)(endtime))
    };
    typedef eosio::multi_index<N(rungame), rungame> rungames;

    //@abi table 个人游戏历史
    struct historygame
    {
        uint64_t id;
        account_name player1;
        account_name player2;
        asset balance;
        account_name winner;
        time createtime;
        time starttime;
        time endtime;
        uint64_t primary_key() const { return id; }
        EOSLIB_SERIALIZE(historygame, (id)(player1)(player2)(balance)(winner)(createtime)(starttime)(endtime))
    };
    typedef eosio::multi_index<N(historygame), historygame> historygames;

    //@abi table 个人当前游戏
    struct mygame
    {
        uint64_t id;
        account_name player1;
        account_name player2;
        asset balance;
        account_name winner;
        time createtime;
        time starttime;
        time endtime;
        uint64_t primary_key() const { return id; }
        EOSLIB_SERIALIZE(mygame, (id)(player1)(player2)(balance)(winner)(createtime)(starttime)(endtime))
    };
    typedef eosio::multi_index<N(mygame), mygame> mygames;

    void c_sub_balance(account_name owner, asset value)
    {
        accounts from_acnts(_self, owner);
        const auto &from = from_acnts.get(value.symbol.name(), "no balance object found");
        eosio_assert(from.balance.amount >= value.amount, "overdrawn balance");
        from_acnts.modify(from, 0, [&](auto &a) {
            a.balance -= value;
        });
    }

    void c_add_balance(account_name owner, asset value, account_name ram_payer)
    {
        accounts to_acnts(_self, owner);
        auto to = to_acnts.find(value.symbol.name());
        if (to == to_acnts.end())
        {
            to_acnts.emplace(ram_payer, [&](auto &a) {
                a.balance = value;
            });
        }
        else
        {
            to_acnts.modify(to, 0, [&](auto &a) {
                a.balance += value;
            });
        }
    }
    void mtransfer(account_name from, account_name to, asset quantity, account_name ram_payer)
    {
        eosio_assert(is_account(to), "to account does not exist");
        c_sub_balance(from, quantity);
        c_add_balance(to, quantity, ram_payer);
    }

  public:
    // @abi action
    void hi(account_name user);

    // @abi action
    void opennewgame(account_name player, asset quantity);

    // @abi action
    void joingame(uint64_t gameid, account_name player2);

    // @abi action
    void jurgegame(uint64_t gameid, account_name winner);

    // @abi action
    void claimad(account_name account);

    // @abi action
    void erasegame1(account_name player);

    //@abi action
    void deposit(const account_name from, const asset &quantity);

    // @abi action
    void modifystr(account_name receiver);

    // @abi action
    void fetchstr(account_name receiver);

    void withdraw(const account_name account, asset quantity);
    void on(const currency::transfer &t, account_name code);
    void apply(account_name contract, account_name act);
};